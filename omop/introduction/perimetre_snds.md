# Périmètre des données SNDS standardisées
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette documentation concerne la standardisation d'un **échantillon** du SNDS au format OMOP-CDM. Plus précisément, cet échantillon contient des données de patients pour les années 2019-2020, qui ont reçu un diagnostic COVID-19 lors d'une visite à l'hôpital, c'est-à-dire les données du [SNDS Fast-Track](https://www.health-data-hub.fr/catalogue-de-donnees/snds-fast-track-donnees-du-systeme-national-des-donnees-de-sante-pour-les). 

Plus précisément, l'extraction contient : 

- Les tables du DCIR 2019-2020.
- Les tables du PMSI 2019 non consolidées.
- Les tables COVID commençant par C_MCO.

Dans ce cadre, la liste complète des tables et variables du SNDS qui ont été standardisées est disponible dans [ce fichier](/files/tables_variables_snds.xlsx).


