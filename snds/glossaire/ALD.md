# ALD - Affection de Longue Durée
<!-- SPDX-License-Identifier: MPL-2.0 -->

## Ailleurs dans la documentation
- Fiche thématique [bénéficiaires ALD](../fiches/beneficiaires_ald.md)
- Fiche thématique [requête type ALD](../fiches/requete_type_ald.md)
