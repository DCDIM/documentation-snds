# Racines de GHM 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Les racines de <link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " /> regroupent un même type de prise en charge, défini par les diagnostics et les <link-previewer href="acte_classant.html" text="actes classants" preview-title="Acte classant" preview-text="Un acte classant est un acte médico-technique codé dans le résumé d’unité médicale (RUM de chaque patient hospitalisé, d’après la classification commune des actes médicaux CCAM. " /> réalisés au cours du séjour hospitalier. 

Les racines de GHM comportent 5 caractères correspondant : 
- les 2 premiers, à la catégorie majeure de diagnostic (28 <link-previewer href="CMD.html" text="CMD" preview-title="CMD - Catégorie majeure de diagnostic" preview-text="La classification médico-économique utilisée pour le PMSI en MCO est structurée en 28 catégories majeures de diagnostic (CMD. " />), 
- le 3ème, au type d’activité : médecine (M), chirurgie (C), interventionnel hors bloc opératoire (K), indifférencié (Z), 
- les 2 derniers, à un numéro de référence (numéro d’ordre sans signification). 

Elles sont complétées par un 6ème caractère définissant le niveau de sévérité du GHM, de 1 le plus faible à 4 le plus élevé, ou, le cas échéant, sa nature ambulatoire (codé J), ou sa nature de séjour de courte durée (codé T), ou l’absence de niveau de sévérité (Z).

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/les-racines-de-ghm-regroupent-un-meme-type-de) sur le site internet du ministère
