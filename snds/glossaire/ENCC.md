# ENCC - Étude nationale de coûts à méthodologie commune
<!-- SPDX-License-Identifier: MPL-2.0 -->

L’ENCC concerne le champ <link-previewer href="MCO.html" text="MCO" preview-title="MCO - Médecine, chirurgie, obstétrique" preview-text="Terme utilisé pour désigner les activités aigus de courte durée réalisées dans les établissements de santé, en hospitalisation (avec ou sans hébergement) ou en consultations externes." /> et le champ <link-previewer href="HAD.html" text="HAD" preview-title="HAD - Hospitalisation à domicile" preview-text="L’hospitalisation à domicile (HAD) est une hospitalisation à temps complet au cours de laquelle les soins sont effectués au domicile de la personne. " />. 

En MCO, l’ENCC a succédé à l’étude nationale des coûts en vigueur depuis 1996 dans les établissements <link-previewer href="ex-DG.html" text="ex-DG" preview-title="Ex-DG - ex dotation globale" preview-text="Néologisme désignant les établissements financés par la dotation globale, avant la mise en place de la tarification à l’activité[^1]." /> et à l’étude des coûts menée en 2004 et 2005 dans le secteur <link-previewer href="ex-OQN.html" text="ex-OQN" preview-title="Ex-OQN - ex objectif quantifié national" preview-text="Néologisme désignant les établissements du secteur privé anciennement sous objectif quantifié national[^1]." />. 

Elle s’appuie sur un échantillon d’établissements de santé volontaires pour : 
- construire une échelle de coûts par <link-previewer href="GHM.html" text="groupe homogène de malades" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />, et hiérarchiser les tarifs des établissements selon le système de la <link-previewer href="T2A.html" text="tarification à l’activité" preview-title="T2A - Tarification à l'activité" preview-text="La tarification à l'activité (T2A) est un mode de financement des établissements de santé français issu de la réforme hospitalière du plan Hôpital 2007, qui vise à médicaliser le financement tout en équilibrant l'allocation des ressources financières et en responsabilisant les acteurs de santé. " /> ; 
- publier des coûts annuels moyens par groupe homogène de malades, décomposés par grands postes de charges, auxquels les établissements peuvent comparer leurs propres coûts, pour leur gestion interne ;
- constituer une base de données utilisée pour les travaux assurant l’évolution des <link-previewer href="classification_medico_economique.html" text="classifications médico-économiques" preview-title="Classification médico-économique" preview-text="La classification médico-économique consiste à classer des pathologies en groupes cohérents d’un point de vue médical et en termes de coûts. " />.

En HAD, l’ENCC a été développée en cohérence avec la méthodologie ENCC du champ MCO et a fait l’objet d’un guide méthodologique en mars 2009 (disponible comme le guide de l’ENCC MCO sur le site de l’ATIH).

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/etude-nationale-de-couts-a-methodologie-commune-encc) sur le site internet du ministère
